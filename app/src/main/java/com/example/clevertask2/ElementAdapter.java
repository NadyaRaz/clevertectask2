package com.example.clevertask2;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ElementAdapter extends RecyclerView.Adapter<ElementAdapter.Holder> {

    private List<ElementDto> list;
    private IElementAdapterListener listener;

    public ElementAdapter(List<ElementDto> list, IElementAdapterListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_elem, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.populate(list.get(position), listener);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.image.setTransitionName(holder.view.getContext().getResources().getString(R.string.image_transaction_name)+position);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {

        private TextView title;
        private TextView description;
        private View view;
        private ImageView image;

        public Holder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            description = itemView.findViewById(R.id.decsription);
            view = itemView;
            image = itemView.findViewById(R.id.icon_view);
        }

        void populate(final ElementDto elementDto, final IElementAdapterListener listener) {
            title.setText(elementDto.getTitle());
            description.setText(elementDto.getDescription());
            view.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    listener.onClick(elementDto, image, ViewCompat.getTransitionName(image));
                }
            });
        }
    }
}
