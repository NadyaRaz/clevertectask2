package com.example.clevertask2;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new FirstFragment()).commit();
    }


    public void openSecondFragment(ElementDto elementDto, ImageView image, String transName){
        getSupportFragmentManager()
                .beginTransaction()
                .addSharedElement(image, ViewCompat.getTransitionName(image))
                .addToBackStack(FirstFragment.class.getName())
                .replace(R.id.fragment_container, new SecondFragment().newInstance(elementDto, transName))
                .commit();
    }

}
