package com.example.clevertask2;

import android.widget.ImageView;

public interface IElementAdapterListener {

    void onClick(ElementDto elementDto, ImageView image, String transName);
}
