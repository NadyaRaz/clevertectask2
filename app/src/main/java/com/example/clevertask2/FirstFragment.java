package com.example.clevertask2;

import android.os.Build;
import android.os.Bundle;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class FirstFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSharedElementEnterTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move));
            setSharedElementReturnTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move));
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ArrayList<ElementDto> list = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            list.add(new ElementDto(getResources().getString(R.string.title) + " " + (i + 1),
                    getResources().getString(R.string.description) + " " + (i + 1)));
        }
        ((RecyclerView) view.findViewById(R.id.recycler_view)).setAdapter(
                new ElementAdapter(
                        list,
                        new IElementAdapterListener() {
                            @Override
                            public void onClick(ElementDto elementDto, ImageView image, String transName) {
                                ((MainActivity) getActivity()).openSecondFragment(elementDto, image, transName);
                            }
                        }
                )
        );
    }
}
