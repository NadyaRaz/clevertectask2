package com.example.clevertask2;

import android.os.Build;
import android.os.Bundle;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

public class SecondFragment extends Fragment {

    private static String ELEMENT_DTO_TITLE = "my_element_dto_title_key";
    private static String ELEMENT_DTO_DESCRIPTION = "my_element_dto_description_key";
    private static String TRANS_NAME = "my_trans_name_key";


    public static SecondFragment newInstance(ElementDto elementDto, String transName) {

        Bundle args = new Bundle();
        args.putString(TRANS_NAME, transName);
        args.putString(ELEMENT_DTO_TITLE, elementDto.getTitle());
        args.putString(ELEMENT_DTO_DESCRIPTION, elementDto.getDescription());

        SecondFragment fragment = new SecondFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSharedElementReturnTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move));
            setSharedElementEnterTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move));
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.findViewById(R.id.icon_view).setTransitionName(getArguments().getString("my_trans_name_key"));
        }
        ((TextView) view.findViewById(R.id.title)).setText(getArguments().getString("my_element_dto_title_key"));
        ((TextView) view.findViewById(R.id.decsription)).setText(getArguments().getString("my_element_dto_description_key"));
        ((Toolbar) view.findViewById(R.id.toolbar)).setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        view.findViewById(R.id.finish_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }
}
